var products=[];
var saveforLater=[];

function getProducts() {
    
    return fetch("http://localhost:3000/products",
    {
        method:"GET"
    }).then((res) => 
    {
        if(res.ok) {
            return res.json();
        }
        else {
            return Promise.reject(res.status)
        }
    }).then((x) => {
        products = x
        showProducts(products)
        return x
    })

}


let showProducts = (x) => {

    x.forEach(element => {
        document.getElementById("prod").innerHTML+= 
    `<div class="divProducts">
    <div class="card" style="width: 25rem;">
    <img class="card-img-top" src=${element.thumbnail} alt="${element.brand}">
    <div class="card-body">
    <p class="card-title" >
    <dt><h5>${element.title}</h5></dt>
    </p>
    <p class="card-text" >
    <dd><h6>${element.description}</d6></dd>
    <dd><h6>${element.price}</h6></ddd
    <dd><h6>${element.rating}</h6></dd>
    <dd><h6>${element.stock}</h6></ddd
    <dd><h6>${element.category}</h6></dd>
    <button type="button" class="btn btn-success" id="but" onclick="addForLater(${element.id})">Save For Later</button>
    </p>
    </div>
    </div> 
    </div><br>
    `
})
}

function getSaveForLater() {

 return fetch("http://localhost:3000/saveforLater",
 {
    method:"GET"
 }).then((res) =>
 {
    if(res.ok) {
       return res.json()
    }
    else {
            return Promise.reject(res.status)
    }
 }).then((x) => {
    saveforLater = x
    showSavedforLater(x)
    return x
})
}

function addForLater(id) {
    let obj=products.find(x =>
        {
            if(x.id == id) {
                return x
            }
        })

let save=saveforLater.find(y => {
    if(y.id == obj.id) {
        return y
    }
})

if(save) {
    alert("Product is already added.");
    return Promise.reject(new Error("Product is already added to save for later."));
}
else {
    return fetch("http://localhost:3000/saveforLater",
    {
        method:"POST",
        headers: {'content-type' : 'application/json'},
        body: JSON.stringify(obj)
    }).then(res => {
        if(res.ok) {
            return res.json();
        }
    }).then(x => {
        saveforLater.push(x);
        showSavedforLater(saveforLater);
        return saveforLater;
    }
    )
}
}

let showSavedforLater = (x) => {
    x.forEach(element => {
        document.getElementById("later").innerHTML+= 
        `<div class="divProducts">
        <div class="card" style="width: 25rem;">
        <img class="card-img-top" src=${element.thumbnail} alt="${element.brand}">
    <div class="card-body">
    <p class= "card-title">
        <dt><h5>${element.title}</h5></dt>
        </p>
        <p class= "card-text">
    <dd><h6>${element.description}</h6></dd>
    <dd><h6>${element.price}</h6></dd>
    <dd><h6>${element.rating}</h6></dd>
    <dd><h6>${element.stock}</h6></dd>
    <dd><h6>${element.category}</h6></dd>
    <button type="button" class="btn btn-danger" onclick="delForLater(${element.id})">Delete from Later</button>
    </p>
    </div>
    </div>
    </div><br>
    `
    })
}

function delForLater(id) {
    
    return fetch(`http://localhost:3000/saveforLater/${id}`,
    { 
        method: "DELETE"
    })
    .then(res => {
        if(res.ok){
            return res.json();
        }
    }
    )
    .then(x => {
         saveforLater.splice(id);
         showProducts(saveforLater);
         return saveforLater;
}
)
}




